package com.example.Spring.RESTful.rest.restcontroller;

import com.example.Spring.RESTful.respository.BookRespository;
import com.example.Spring.RESTful.respository.CategoryRespository;
import com.example.Spring.RESTful.respository.dto.Bookdto;
import com.example.Spring.RESTful.respository.dto.Category;
import com.example.Spring.RESTful.rest.request.BookRestModel;
import com.example.Spring.RESTful.rest.request.CategoryRestModel;
import com.example.Spring.RESTful.rest.response.BaseApiResponse;
import com.example.Spring.RESTful.rest.response.Messages;
import com.example.Spring.RESTful.rest.utils.CommonUtil;
import com.example.Spring.RESTful.service.CategoryService;
import com.example.Spring.RESTful.service.impl.BookServiceImpl;
import com.example.Spring.RESTful.service.impl.CategoryServicelmpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;
@RestController
public class CategoryRestController {

        private CategoryService categoryService;
        private CategoryRespository categoryRespository;
        private CommonUtil commonUtil;
        @Autowired
        public void setCategoryService(CategoryService categoryService) {
            this.categoryService = categoryService;
        }

        @Autowired
        public void setUtils(CommonUtil commonUtil) {
            this.commonUtil=commonUtil;
        }



        @PostMapping("/category")
        public ResponseEntity<BaseApiResponse<CategoryRestModel>> postCategory(@RequestBody CategoryRestModel bookRequest) {
            Category category=commonUtil.getMapper().map(bookRequest,Category.class);
            CategoryRestModel bookResponse=commonUtil.getMapper().map( categoryService.postCategory(category),CategoryRestModel.class);
            BaseApiResponse<CategoryRestModel> responseMessage=new BaseApiResponse<>();
            responseMessage.setData(bookResponse);
            responseMessage.setMessage(Messages.Success.INSERT_SUCCESS.getMessage());
            responseMessage.setStatus(HttpStatus.CREATED);
            responseMessage.setTime(CommonUtil.getCurrentTime());
            return ResponseEntity.ok(responseMessage);
        }

        @GetMapping("/category")
        public ResponseEntity<BaseApiResponse<List<Category>>> getAllCategories(){
            System.out.println("dd");
            ModelMapper mapper = new ModelMapper();
            BaseApiResponse<List<Category>> baseApiResponse =
                    new BaseApiResponse<>();

            List<Category> articleDtoList = categoryService.getAllCategories();

            baseApiResponse.setMessage("You have found all articles successfully");
            baseApiResponse.setData(articleDtoList);
            baseApiResponse.setStatus(HttpStatus.OK);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));

            return ResponseEntity.ok(baseApiResponse);
        }



        @GetMapping("/category/{id}")
        public ResponseEntity<BaseApiResponse<Category>> getCategoryById(@PathVariable int id){
            ModelMapper mapper = new ModelMapper();
            BaseApiResponse<Category> baseApiResponse =
                    new BaseApiResponse<>();

            Category articleDtoList = categoryService.getCategoryById(id);

            baseApiResponse.setMessage("You have found all articles successfully");
            baseApiResponse.setData(articleDtoList);
            baseApiResponse.setStatus(HttpStatus.OK);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));

            return ResponseEntity.ok(baseApiResponse);
        }



        @PutMapping("/category/{id}")
        public ResponseEntity<BaseApiResponse<CategoryRestModel>> updateCategory(@PathVariable int id,@RequestBody BookRestModel newBook){
            BaseApiResponse<CategoryRestModel> response = new BaseApiResponse<>();
            System.out.println("" +newBook);

           Category book = commonUtil.getMapper().map(newBook,Category.class);
          Category category = categoryService.updateCategory(id,book);
            System.out.println(category);

         CategoryRestModel bookRequest = commonUtil.getMapper().map(category,CategoryRestModel.class);

            response.setMessage("You have updated a book!");
            response.setData(bookRequest);
            response.setStatus(HttpStatus.OK);
            response.setTime(commonUtil.getCurrentTime());

            return ResponseEntity.ok(response);
        }

        @DeleteMapping("/category/{id}")
        public ResponseEntity<String> deleteBook(@PathVariable int id){

            String message = categoryService.deleteCategory(id);

            return ResponseEntity.ok(message);
        }





}
