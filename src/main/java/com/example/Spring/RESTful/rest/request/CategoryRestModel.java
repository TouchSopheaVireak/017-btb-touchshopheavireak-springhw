package com.example.Spring.RESTful.rest.request;

public class CategoryRestModel {

    private  String title;

    CategoryRestModel(){

    }
    public CategoryRestModel(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "CategoryRestModel{" +
                "title='" + title + '\'' +
                '}';
    }
}
