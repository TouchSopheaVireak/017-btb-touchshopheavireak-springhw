package com.example.Spring.RESTful.rest.restcontroller;

//import com.example.Spring.RESTful.pagination.Pagination;
import com.example.Spring.RESTful.respository.BookRespository;
import com.example.Spring.RESTful.respository.dto.Bookdto;
import com.example.Spring.RESTful.rest.request.BookRestModel;
import com.example.Spring.RESTful.rest.response.BaseApiResponse;
import com.example.Spring.RESTful.rest.response.Messages;
import com.example.Spring.RESTful.rest.utils.CommonUtil;
import com.example.Spring.RESTful.service.impl.BookServiceImpl;
import org.apache.logging.log4j.message.Message;
import org.modelmapper.ModelMapper;
import org.postgresql.core.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


@RestController
public class BookRestController {

    private BookServiceImpl bookService;
    private BookRespository bookRespository;
    private CommonUtil commonUtil;

    @Autowired
    public void setBookService(BookServiceImpl bookService) {
        this.bookService = bookService;
    }

    @Autowired
    public void setUtils(CommonUtil commonUtil) {
        this.commonUtil=commonUtil;
    }
    @Autowired
    public BookRestController(BookRespository bookRespository) {
        this.bookRespository = bookRespository;
    }

    @PostMapping("/Book")
    public ResponseEntity <BaseApiResponse<BookRestModel>> InsertBook(@RequestBody BookRestModel bookRequest) {
      Bookdto bookDto=commonUtil.getMapper().map(bookRequest,Bookdto.class);
        BookRestModel bookResponse=commonUtil.getMapper().map( bookService.insertBook(bookDto),BookRestModel.class);
        BaseApiResponse<BookRestModel> responseMessage=new BaseApiResponse<>();
        responseMessage.setData(bookResponse);
        responseMessage.setMessage(Messages.Success.INSERT_SUCCESS.getMessage());
        responseMessage.setStatus(HttpStatus.CREATED);
        responseMessage.setTime(CommonUtil.getCurrentTime());
        return ResponseEntity.ok(responseMessage);
    }



    @GetMapping("/Book")
    public ResponseEntity<BaseApiResponse<List<Bookdto>>> findAll( ){
        ModelMapper mapper = new ModelMapper();
        BaseApiResponse<List<Bookdto>> baseApiResponse =
                new BaseApiResponse<>();

        List<Bookdto> articleDtoList = bookService.findAll();

        baseApiResponse.setMessage("You have found all articles successfully");
        baseApiResponse.setData(articleDtoList);
        baseApiResponse.setStatus(HttpStatus.OK);
        baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(baseApiResponse);
    }

    @GetMapping("/Book/{id}")
    public ResponseEntity<BaseApiResponse<Bookdto>> findOneById(@PathVariable int id){
        ModelMapper mapper = new ModelMapper();
        BaseApiResponse<Bookdto> baseApiResponse =
                new BaseApiResponse<>();

        Bookdto articleDtoList = bookService.findOneById(id);

        baseApiResponse.setMessage("You have found all articles successfully");
        baseApiResponse.setData(articleDtoList);
        baseApiResponse.setStatus(HttpStatus.OK);
        baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(baseApiResponse);
    }



    @PutMapping("/Book/{id}")
    public ResponseEntity<BaseApiResponse<BookRestModel>> updateBook(@PathVariable int id,@RequestBody BookRestModel newBook){
        BaseApiResponse<BookRestModel> response = new BaseApiResponse<>();
        System.out.println("" +newBook);

        Bookdto book = commonUtil.getMapper().map(newBook,Bookdto.class);
        Bookdto bookDto = bookService.updateBook(id,book);
        System.out.println(bookDto);

        BookRestModel bookRequest = commonUtil.getMapper().map(bookDto,BookRestModel.class);

        response.setMessage("You have updated a book!");
        response.setData(bookRequest);
        response.setStatus(HttpStatus.OK);
        response.setTime(commonUtil.getCurrentTime());

        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/Book/{id}")
    public ResponseEntity<String> deleteBook(@PathVariable int id){

        String message = bookService.deleteBook(id);

        return ResponseEntity.ok(message);
    }




}