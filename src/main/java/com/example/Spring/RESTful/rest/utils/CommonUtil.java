package com.example.Spring.RESTful.rest.utils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
@Component
public class CommonUtil {

    public static ModelMapper getMapper() {
        return new ModelMapper();
    }

    public static Timestamp getCurrentTime() {
        return new Timestamp(System.currentTimeMillis());
    }

}
