package com.example.Spring.RESTful.service;

import com.example.Spring.RESTful.respository.dto.Bookdto;

import java.util.List;

public interface BookService {



    List<Bookdto>  findAll();
    Bookdto insertBook(Bookdto bookdto);
    String deleteBook(int Id);
    Bookdto updateBook(int Id,Bookdto bookdto);
    Bookdto findOneById(int Id);
//    Bookdto insert(Bookdto bookdto);



}
