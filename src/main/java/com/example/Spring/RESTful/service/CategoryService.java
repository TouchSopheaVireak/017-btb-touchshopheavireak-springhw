package com.example.Spring.RESTful.service;

import com.example.Spring.RESTful.respository.dto.Bookdto;
import com.example.Spring.RESTful.respository.dto.Category;

import java.util.List;

public interface CategoryService {



    List<Category> getAllCategories();
    Category postCategory(Category category);
    String deleteCategory(int Id);
    Category updateCategory(int Id,Category category);
    Category getCategoryById(int Id);

//    int countAllCategory();
}
