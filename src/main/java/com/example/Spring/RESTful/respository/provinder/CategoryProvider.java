package com.example.Spring.RESTful.respository.provinder;

import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {

    public String getAllCategories(){
        return new SQL(){{
            SELECT("*");
            FROM("categories");
        }}.toString();
    }

    public String getCategoryById(){
        return new SQL(){{
            SELECT("*");
            FROM("categories");
            WHERE("id=#{id}");
        }}.toString();
    }

    public String postCategory(){
        return new SQL(){{
            INSERT_INTO("categories");
            VALUES("title","#{title}");
        }}.toString();
    }

    public String getCategoryIdByTitle(){
        return new SQL(){{
            SELECT("*");
            FROM("categories");
            WHERE("title=#{title}");
        }}.toString();
    }

    public String updateCategory(){
        return new SQL(){{
            UPDATE("categories");
            SET("title=#{category.title}");
            WHERE("id=#{id}");
        }}.toString();
    }

    public String deleteCategory(){
        return new SQL(){{
            DELETE_FROM("categories");
            WHERE("id=#{id}");
        }}.toString();
    }



}
